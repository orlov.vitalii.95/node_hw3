const express = require('express')
const router = express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {getTruck, getTrucks, addTruck, deleteTruck, updateTruck, assignTruck} = require('../controllers/truckController')

router.get('/', authMiddleware, getTrucks);
router.post('/', authMiddleware, addTruck);
router.get('/:id', authMiddleware, getTruck);
router.put('/:id', authMiddleware, updateTruck);
router.delete('/:id', authMiddleware, deleteTruck);
router.post('/:id/assign', authMiddleware, assignTruck);

module.exports = router
