const express = require('express')
const router = express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {getProfileInfo, deleteUser, changePassword} = require('../controllers/userController');

router.get('/me', authMiddleware, getProfileInfo);
router.patch('/me/password', authMiddleware, changePassword);
router.delete('/me', authMiddleware, deleteUser);

module.exports = router


