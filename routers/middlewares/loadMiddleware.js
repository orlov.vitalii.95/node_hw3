const { Load } = require('../../models/loadModel')
const truckTypes = [{
    name: 'SPRINTER',
    width: 300,
    length: 250,
    height: 170,
    payload: 1700
}, {
    name: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    payload: 2500
}, {
    name: 'LARGE STRAIGHT',
    width: 700, 
    length: 350,
    height: 200,
    payload: 4000
}]

module.exports.loadMiddleware = async (req, res, next) => {
    try {
    const id = req.params.id;
    const load = await Load.findOne({_id: id});
    const truck = truckTypes.find(item => {
        if (+item.width < load.dimensions.width
            || +item.height < load.dimensions.height
            || +item.length < load.dimensions.length
            || +item.payload < load.payload) {
                return false
            }

        return true 
    })

    req.truckType = truck.name;
    req.load = load;
    next();

    } catch (e) {
        console.log(e)
        res.status(500).json({message: 'Internal server error'})
    }
}