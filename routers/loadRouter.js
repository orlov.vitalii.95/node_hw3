const express = require('express')
const router = express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const { validateLoad} = require('./middlewares/loadValidationMiddleware');
const {loadMiddleware} = require('./middlewares/loadMiddleware');
const {addLoad, postLoad, updateLoad, deleteLoad, getActiveLoad, getShippingInfo, getLoads, iterateState, getLoad} = require('../controllers/loadController');

router.get('/', authMiddleware, getLoads);
router.post('/', authMiddleware, validateLoad, addLoad);
router.get('/active', authMiddleware, getActiveLoad);
router.patch('/active/state', authMiddleware, iterateState);
router.get('/:id', authMiddleware, getLoad);
router.put('/:id', authMiddleware, loadMiddleware, updateLoad);
router.delete('/:id', authMiddleware, loadMiddleware, deleteLoad);
router.post('/:id/post', authMiddleware, loadMiddleware, postLoad),
router.get('/:id/shipping_info', authMiddleware, loadMiddleware, getShippingInfo)

module.exports = router;


