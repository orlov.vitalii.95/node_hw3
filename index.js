const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter')

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter)

// class UnauthorizedError extends Error {
//     constructor(message = 'Unauthorized user!') {
//         super(message);
//         statusCode = 401;
//     }
// }

// app.use((err, req, res, next) => {
//     if (err instanceof UnauthorizedError) {
//         return res.status(err.statusCode).json({message: err.message});
//     }
//     return res.status(500).json({message: err.message});
// });

const start = async () => {
    await mongoose.connect('mongodb+srv://testuser:YqKGq6RaOXX1og4p@cluster0.dn9os.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    });

    app.listen(port, () => {
        console.log(`Server works at port ${port}!`);
    });
};

start();

