const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
    created_by: {
        type: String,
        required: true
    },
    assigned_to: {
        type: String
    },
    status: {
        type: String,
        required: true
    },
    state: {
        type: String,
    },
    name: {
        type: String,
        required: true
    },
    payload: {
        type: Number,
        required: true
    },
    pickup_address: {
        type: String,
        required: true
    },
    delivery_address: {
        type: String,
        required: true
    },
    dimensions: {
        type: Object,
        required: true
    },
    logs: {
        type: Array,
        required: true
    },
    driver_id: {
        type: String
    },
    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports.Load = mongoose.model('Load', loadSchema);