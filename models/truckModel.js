const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
    type: {
        type: String,
        required: true
    },
    created_by: {
        type: String,
        required: true
    },
    assigned_to: {
        type: String,
    },
    status: {
        type: String,
        required: true
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});

module.exports.Truck = mongoose.model('Truck', truckSchema);