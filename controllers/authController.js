const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { JWT_SECRET } = require('../config');
const { User } = require('../models/userModel');

module.exports.registration = async (req, res) => {
    try {
        const { email, password, role } = req.body;
        
        const isUserExists = await User.findOne({email: email});
        console.log(isUserExists)
        
        if (isUserExists) {
            return res.status(400).json({message: `User with username ${email} has been already created!`})
        }
        
        if (!password) {
            return res.status(400).json({message: `Password is required!`})
        }
        
        if (!role) {
            return res.status(400).json({message: `Role is required!`})
        }
        
        const user = new User({
            email: email,
            password: await bcrypt.hash(password, 10),
            role
        });
        
        await user.save();
        
        res.status(200).json({message: 'Success'});
    } catch (e) {
        res.status(500).json({message: `Internal server erroe`})
    }
};

module.exports.login = async (req, res) => {
    try {
        const { email, password } = req.body;
        
        const user = await User.findOne({email: email});
        
        if (!user) {
            return res.status(400).json({message: `No user with username '${email}' found!`});
        }
        
        if ( !(await bcrypt.compare(password, user.password)) ) {
            return res.status(400).json({message: `Wrong password!`});
        }
        
        const token = jwt.sign({ email: user.email, _id: user._id, role: user.role }, JWT_SECRET);
        res.json({message: 'Success', jwt_token: token});
    } catch (e) {
        res.status(500).json({message: `Internal server erroe`})
    }
};