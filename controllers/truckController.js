const { Truck } = require('../models/truckModel');

module.exports.addTruck = async (req, res) => {
    
    try {
        const type = req.body.type;
        
        if (!type) {
            return res.status(400).json({message: `Please, specify a truck type!`})
        }
        
        const truck = new Truck({
            type,
            created_by: req.user._id,
            assigned_to: null,
            status: 'IS',
        })
        
        await truck.save();
        
        res.status(200).json({message: 'Truck created successfully'})
    } catch (e) {
        console.log(e)
        res.status(500).json({message: 'Internal server error'})
    }
}

module.exports.getTrucks = async (req, res) => {
    try {

        console.log(req.user)
                
        if (req.user.role !== 'DRIVER') {
            return res.status(400).json({message: `Trucks in only available for drivers!`})
        }
        
        const trucks = await Truck.find({created_by: req.user._id});
        return res.status(200).json({trucks : [...trucks]})
        
    } catch (e) {
        console.log(e)
        return res.status(500).json({message: `Internal server error`})
    }
}

module.exports.getTruck = async (req, res) => {
    try {
        
        if (req.user.role !== 'DRIVER') {
            return res.status(400).json({message: `Trucks in only available for drivers!`})
        }
        
        const truck = await Truck.findOne({_id: req.params.id})
        res.status(200).json({truck: truck})
        
    } catch (e) {
        return res.status(500).json({message: `Internal server error`})
    }
}

module.exports.updateTruck = async (req, res) => {
    try {   
        const truckId = req.params.id;
        
        if (req.user.role !== 'DRIVER') {
            return res.status(400).json({message: `Trucks in only available for drivers!`})
        }
        
        const keys = Object.keys(req.body);
        const truck = await Truck.findOne({_id: truckId});
        
        if (truck.created_by !== req.user._id
            || truck.status === 'OL') {
                return res.status(400).json({message: "You can't update truck that is on load"})
        }

        if (keys.length < 1) {
            return res.status(400).json({message: `Please, specify details!`})
        }
        
        keys.forEach(item => truck[item] = req.body[item]);
        
        await truck.save();
        
        res.status(200).json({message: `Truck details changed successfully`})
    } catch (e) {
        console.log(e)
        res.status(500).json({message: `Internal server error`})
    }
} 

module.exports.deleteTruck = async (req, res) => {
    try {
        
        if (req.user.role !== 'DRIVER') {
            return res.status(400).json({message: `Trucks in only available for drivers!`})
        }
        
        await Truck.deleteOne({_id: req.params.id});
        res.status(200).json({message: `Truck deleted successfully`});
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
}

module.exports.assignTruck = async (req, res) => {
    try {
        
        if (req.user.role !== 'DRIVER') {
            return res.status(400).json({message: `Trucks in only available for drivers!`})
        }

        const assignedTruck = await Truck.findOne({assigned_to: req.user._id});
        
        if (assignedTruck) {
            await Truck.updateOne({_id: assignedTruck._id}, {assigned_to: null})
        }

        await Truck.updateOne({_id: req.params.id}, {assigned_to: req.user._id})
        res.status(200).json({message: `Truck assigned successfully`})
    } catch (e) {
        res.status(500).json({message: `Internal server error`})
    }
}

