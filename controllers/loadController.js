const { Load } = require('../models/loadModel');
const { Truck } = require('../models/truckModel')
const states = ['En route to Pick up', `Arrived to Pick up`, `En route to delivery`, `Arrived to delivery`]

module.exports.addLoad = async (req, res) => {
    try {
        
        if (req.user.role !== 'SHIPPER') {
            return res.status(400).json({message: `This action is only available for SHIPPERs!`})
        }
        
        const load = new Load({
            name: req.body.name,
            payload: req.body.payload,
            pickup_address: req.body.pickup_address,
            delivery_address: req.body.delivery_address,
            dimensions : req.body.dimensions,
            created_by: req.user._id,
            status: 'NEW',
            state: '',
            logs: [{
                message: `Load was created`,
                time: new Date()  
            }]    
        });
        
        await load.save()
        res.status(200).json({message: 'Load created succesfully!'})
    } catch(e) {
        console.log(e)
        res.status(500).json({message: `Internal server error`})
    }
}

module.exports.postLoad = async (req, res) => {
    
    try {
        
        if (req.user.role != 'SHIPPER') {
            return res.status(400).json({message: `This action is only available for SHIPPERs!`})
        }
        
        const load = req.load;
        await Load.updateOne({_id: load._id}, {status: 'POSTED'});
        const truck = await Truck.findOne({status: 'IS', type: req.truckType});
        
        if (!truck) {
            await Load.updateOne({_id: load._id}, {status: 'NEW'})
            return res.status(200).json({
                message: "Load posted successfully",
                driver_found: false
            })
        } else {
            const logs = load.logs;
            logs.push({
                message: `Load assigned to driver with id ${truck.assigned_to}`,
                time: new Date()
            })
            await Load.updateOne({_id: load._id}, {
                status: 'ASSIGNED',
                assigned_to: truck._id,
                state: states[0],
                driver_id: truck.assigned_to,
                logs: logs});
                
                await Truck.updateOne({_id: truck._id}, {status: 'OL'})
                
                return res.status(200).json({
                    message: "Load posted successfully",
                    driver_found: true
                })
                
            }
            
        } catch (e) {
            console.log(e)
            return res.status(500).json({message: 'Internal server error'})
        }    
    }
    
    module.exports.deleteLoad = async (req, res) => {
        try {
            const load = req.load;
            
            if (req.user.role != 'SHIPPER') {
                return res.status(400).json({message: `This action is only available for SHIPPERs!`})
            }
            
            if (load.created_by !== req.user._id
                || load.status !== 'NEW') {
                    return res.status(400).json({message: `You can't delete this load!`})
                }
                
                await Load.deleteOne({_id: load._id});
                return res.status(200).json({message: 'Load deleted successfully'})
                
            } catch (e) {
                return res.status(500).json({message: 'Internal server error'})
                
            }
        }
        
        module.exports.updateLoad = async (req, res) => {
            try {
                const load = req.load;
                
                if (req.user.role != 'SHIPPER') {
                    return res.status(400).json({message: `This action is only available for SHIPPERs!`})
                }
                
                if (load.created_by !== req.user._id
                    || load.status !== 'NEW') {
                        return res.status(400).json({message: `You can't delete this load!`})
                    }
                    
                    await Load.updateOne({_id: load._id}, {...req.body})
                    
                    return res.status(200).json({message: 'Load details changed successfully'})
                } catch (e) {
                    return res.status(500).json({message: 'Internal server error'})
                }
            }
            
            module.exports.getActiveLoad = async (req, res) => {
                try {
                    const driverId = req.user._id;
                    
                    if (req.user.role != 'DRIVER') {
                        return res.status(400).json({message: `This action is only available for drivers!`})
                    }
                    
                    const load = await Load.findOne({driver_id: driverId, status: 'ASSIGNED'})
                    
                    if (!load) {
                        return res.status(200).json({message: `You don't have active loads`})
                    }
                    
                    return res.status(200).json({load: load})
                } catch (e) {
                    return res.status(500).json({message: 'Internal server error'})
                }
            }
            
            module.exports.getShippingInfo = async (req, res) => {
                try {
                    const load = req.load;
                    
                    if (req.user.role != 'SHIPPER') {
                        return res.status(400).json({message: `This action is only available for SHIPPERs!`})
                    }
                    
                    if (load.created_by !== req.user._id) {
                        return res.status(400).json({message: `You can only get info about your shippings`})
                    }
                    
                    const truck = await Truck.findOne({_id: load.assigned_to})
                                        
                    return res.status(200).json({...load._doc, truck})
                    
                } catch (e) {
                    return res.status(500).json({message: 'Internal server error'})
                }
            }
            
            module.exports.getLoads = async (req, res) => {
                try {
                    const status = req.params.status;
                    const limit = req.params.limit || 10;
                    const offset = req.params.offset || 0;
                    
                    if (req.user.role === 'SHIPPER') {
                        if (status) {
                            const loads = await Load.aggregate([
                                {$match: {created_by:req.user._id, status}},
                                {$skip: +offset},
                                {$limit: +limit}
                            ])
                            return res.status(200).json({loads: [...loads]})
                        } else {
                            const loads = await Load.aggregate([
                                {$match: {created_by: req.user._id}},
                                {$skip: +offset},
                                {$limit: +limit}
                            ])
                            console.log(1, loads)
                            return res.status(200).json({loads: [...loads]})
                        }
                    } else {
                        if (status) {
                            const loads = await Load.aggregate([
                                {$match: {driver_id:req.user._id, status}},
                                {$skip: +offset},
                                {$limit: +limit}
                            ])
                            return res.status(200).json({loads: [...loads]})
                        } else {
                            const loads = await Load.aggregate([
                                {$match: {driver_id: req.user._id}},
                                {$skip: +offset},
                                {$limit: +limit}
                            ])
                            
                            return res.status(200).json({loads: [...loads]})
                        }
                    }
                    
                    
                } catch (e) {
                    return res.status(500).json({message: 'Internal server error'})
                }
            }
            
            module.exports.getLoad = async (req, res) => {
                try {
                    const load = await Load.findOne({_id: req.params.id})
                    return res.status(200).json({load})
                } catch (e) {
                    return res.status(500).json({message: 'Internal server error'})
                }
            }
            
            module.exports.iterateState = async (req, res) => {
                try {
                    
                    if (req.user.role != 'DRIVER') {
                        return res.status(400).json({message: `This action is only available for drivers!`})
                    }
                    
                    const load = await Load.findOne({driver_id: req.user._id, status: 'ASSIGNED'});
                    const stateIndex = states.indexOf(load.state);
                    const logs = load.logs
                    
                    switch (stateIndex) {
                        case 2: 
                        load.state = states[3];
                        load.status = 'SHIPPED';
                        logs.push({
                            message: `Load is ${states[3]}`,
                            time: new Date()
                        })
                        
                        await Load.updateOne({_id: load._id}, {
                            state: states[3],
                            status: 'SHIPPED',
                            logs: logs
                            
                        });
                        await Truck.updateOne({_id: load.assigned_to}, {status: 'IS'})
                        return res.status(200).json({message: `Load state changed to ${states[3]}`})
                        case 3:
                        return res.status(400).json({message: 'This load has been already shipped!'})
                        default:
                        logs.push({
                            message: `Load is ${states[stateIndex + 1]}`,
                            time: new Date()
                        })
                        
                        await Load.updateOne({_id: load._id}, {
                            state: states[stateIndex + 1],
                            logs: logs
                        })
                        return res.status(200).json({message: `Load state changed to ${states[stateIndex + 1]}`})

                    }
                } catch (e) {
                    console.log(e)
                    return res.status(500).json({message: 'Internal server error'})
                }
            }